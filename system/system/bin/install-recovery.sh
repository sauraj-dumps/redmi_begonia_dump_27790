#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:9149c9a162712533fab52d0d572aff68e0f39dbf; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:2a2b23cd73e113e44ca1901964e5a900bc2a6899 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:9149c9a162712533fab52d0d572aff68e0f39dbf && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
