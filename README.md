## potato_begonia-userdebug 10 QQ1D.200205.002 51f62c608c release-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: begonia
- Brand: Redmi
- Flavor: potato_begonia-userdebug
- Release Version: 10
- Id: QQ1D.200205.002
- Incremental: 51f62c608c
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/begonia/begonia:9/PPR1.180610.011/V10.4.4.0.PGGCNXM:user/release-keys
- OTA version: 
- Branch: potato_begonia-userdebug-10-QQ1D.200205.002-51f62c608c-release-keys
- Repo: redmi_begonia_dump_27790


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
